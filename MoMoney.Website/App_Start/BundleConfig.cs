﻿using System.Web.Optimization;

namespace MoMoney.Website.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/assets/style").Include
            (
                "~/content/themes/jquery-ui.css",
                "~/content/css/style.css"
            ));

            bundles.Add(new ScriptBundle("~/assets/header").Include
            (
                "~/Scripts/modernizr-2.5.3.js"
            ));

            bundles.Add(new ScriptBundle("~/assets/footer").Include
            (
                "~/Scripts/jquery-1.8.3.js",
                "~/Scripts/jquery-ui-1.9.2.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/handlebars.js",
                "~/Scripts/app.js"
            ));
        }
    }
}