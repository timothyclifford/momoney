﻿var app = app || {};

app.pageNames = {
    getInvoice: "GetInvoice",
    createInvoice: "CreateInvoice",
    editInvoice: "EditInvoice"
};

app.templates = {
    invoiceItem: (function () {
        var template = {};
        return {
            init: function() {
                $.ajax({
                    url: "/content/templates/InvoiceItem.html",
                    type: "GET",
                    async: false,
                    success: function(html) {
                        template = Handlebars.compile(html);
                    }
                });
            },
            exec: function (json) {
                return template(json);
            }
        };
    }())
};

app.models = {
    invoiceItem: function (sender) {
        var container = $(sender).hasClass("invoice-item") ? $(sender) : $(sender).parents(".invoice-item");
        this.Id = container.find(".invoice-item-id").val();
        this.Name = container.find(".invoice-item-name").val();
        this.Description = container.find(".invoice-item-description").val();
        this.Cost = container.find(".invoice-item-cost").val();
        this.Quantity = container.find(".invoice-item-quantity").val();
    }
};

app.util = {
    emptyGuid: "00000000-0000-0000-0000-000000000000",
    isPage: function (ids) {
        for (var i = 0; i < ids.length; i++) {
            if ($("#" + ids[i]).length) {
                return true;
            }
        }
        return false;
    },
    equals: function (int1, int2) {
        return parseInt(int1) === parseInt(int2);
    }
};

app.pages = app.pages || {};

app.pages.invoice = (function () {
    var data = {
        items: []
    };
    var init = function () {
        app.templates.invoiceItem.init();
        if ($("#InvoiceItems").length && $("#InvoiceItems").val().length) {
            data.items = JSON.parse($("#InvoiceItems").val());
            updateTemplate();
        }
        bind();
    };
    var bind = function () {
        $("#Invoice_DateDue").datepicker({
            dateFormat: "dd MM yy",
            setDate: new Date().getMonth() + "/" + new Date().getDate() + "/" + new Date().getFullYear()
        });
        $("#InvoiceItemsContainer").sortable({
            stop: function () {
                update();
            }
        });
        $(".create-invoice-item").live("click", function (e) {
            var invoiceItem = new app.models.invoiceItem(e.target);
            invoiceItem.Id = app.util.emptyGuid;
            $("#InvoiceItemsContainer").append(template(invoiceItem));
            update();
            clearInputs();
        });
        $(".delete-invoice-item").live("click", function (e) {
            $(this).parents(".invoice-item").remove();
            update();
        });
        $("#InvoiceItemsContainer input").blur(function() {
            update();
        });
    };
    var updateTemplate = function () {
        $("#InvoiceItemsContainer").html("");
        var html = "";
        $(data.items).each(function (index, item) {
            html += app.templates.invoiceItem.exec(item);
        });
        $("#InvoiceItemsContainer").html(html);
    };
    var update = function() {
        updateData();
        updateTotal();
    };
    var updateData = function () {
        data.items = [];
        $("#InvoiceItemsContainer ul").each(function (idex, el) {
            data.items.push(new app.models.invoiceItem(el));
        });
        $("#InvoiceItems").val(JSON.stringify(data.items));
    };
    var updateTotal = function () {
        var total = 0;
        $(data.items).each(function (index, item) {
            total += (item.Quantity * item.Cost);
        });
        $(".invoice-total").html(total);
    };
    var clearInputs = function () {
        var parentUl = $(".create-invoice-item").parents("ul.invoice-item");
        $(parentUl).find("input").val("");
    };
    return {
        init: init
    };
}());

$(document).ready(function () {
    app.pages.invoice.init();
});