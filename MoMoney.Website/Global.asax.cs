﻿using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using MoMoney.Website.App_Start;
using Raven.Client;
using Raven.Client.Indexes;

namespace MoMoney.Website
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            BundleTable.EnableOptimizations = true;
            BundleTable.Bundles.UseCdn = true;

            DependencyResolver.SetResolver(new NinjectDependencyResolver());

            IndexCreation.CreateIndexes(Assembly.GetCallingAssembly(), DependencyResolver.Current.GetService<IDocumentStore>());

            ModelValidatorProviders.Providers.Clear();
        }
    }
}