﻿using System.Web.Mvc;

namespace MoMoney.Website.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
