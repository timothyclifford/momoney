﻿using System;
using System.Web.Mvc;
using Raven.Client;

namespace MoMoney.Website.Controllers
{
    public class BaseController : Controller
    {
        protected IDocumentSession RavenSession { get; set; }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            RavenSession = MvcApplication.Store.OpenSession();
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.IsChildAction)
                return;

            using (RavenSession)
            {
                if (filterContext.Exception != null)
                    return;

                if (RavenSession != null)
                    RavenSession.SaveChanges();
            }
        }

        protected T GetById<T>(Guid id)
        {
            return RavenSession.Load<T>(id);
        }
    }
}