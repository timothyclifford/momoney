﻿using System;
using System.Linq;
using System.Web.Mvc;
using MoMoney.Models.Domain;
using MoMoney.Models.View;
using Raven.Client;

namespace MoMoney.Website.Controllers
{
    public class ClientsController : Controller
    {
        private readonly IDocumentSession _ravenSession;

        public ClientsController(IDocumentSession ravenSession)
        {
            _ravenSession = ravenSession;
        }

        public ActionResult Index()
        {
            var clients = _ravenSession.Query<Client>().ToList();
            var clientViewModels = clients.Select(c => new ClientViewModel(c)).ToList();

            return View(clientViewModels);
        }

        public ActionResult Get(Guid id)
        {
            var client = _ravenSession.Load<Client>(id);

            return View(new ClientViewModel(client));
        }

        public ActionResult Create()
        {
            return View(new ClientViewModel());
        }

        [HttpPost]
        public ActionResult Create(ClientViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            _ravenSession.Store(model.Client);

            _ravenSession.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var client = _ravenSession.Load<Client>(id);

            return View(new ClientViewModel(client));
        }

        [HttpPost]
        public ActionResult Edit(ClientViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var client = _ravenSession.Load<Client>(model.Client.Id);

            client.Update(model.Client);

            _ravenSession.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            var client = _ravenSession.Load<Client>(id);

            _ravenSession.Delete(client);

            _ravenSession.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}
