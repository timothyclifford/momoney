﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using MoMoney.Framework;
using MoMoney.Models.Domain;
using MoMoney.Models.View;
using Newtonsoft.Json;
using Raven.Client;

namespace MoMoney.Website.Controllers
{
    public class InvoicesController : Controller
    {
        private readonly IDocumentSession _ravenSession;

        public InvoicesController(IDocumentSession ravenSession)
        {
            _ravenSession = ravenSession;
        }

        public ActionResult Index()
        {
            var invoices = _ravenSession.Query<Invoice>().ToList().Select(i => new InvoiceViewModel(i)).ToList();

            return View(invoices);
        }

        public ActionResult Get(Guid id)
        {
            var invoice = _ravenSession.Load<Invoice>(id);
            var items = _ravenSession.Query<InvoiceItem>().Where(ii => ii.InvoiceId == id).ToList();

            return View(new InvoiceViewModel(invoice, items));
        }

        public ActionResult Create()
        {
            var viewModel = new InvoiceViewModel
            {
                ClientDropDowns = GetClientDropDowns()
            };

            viewModel.Invoice.Number = GetNextInvoiceNumber();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Create(InvoiceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.ClientDropDowns = GetClientDropDowns();
                return View(model);
            }

            _ravenSession.Store(model.Invoice);

            _ravenSession.SaveChanges();

            AddOrUpdateInvoiceItems(model.Invoice.Id, model.InvoiceItems);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(Guid id)
        {
            var invoice = _ravenSession.Load<Invoice>(id);
            var items = _ravenSession.Query<InvoiceItem>().Where(ii => ii.InvoiceId == id).ToList();

            var viewModel = new InvoiceViewModel(invoice, items)
            {
                ClientDropDowns = GetClientDropDowns()
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(InvoiceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.ClientDropDowns = GetClientDropDowns();
                return View(model);
            }

            var invoice = _ravenSession.Load<Invoice>(model.Invoice.Id);

            invoice.Update(model.Invoice);

            _ravenSession.SaveChanges();

            AddOrUpdateInvoiceItems(model.Invoice.Id, model.InvoiceItems);

            return RedirectToAction("Index");
        }

        public ActionResult Delete(Guid id)
        {
            var invoice = _ravenSession.Load<Invoice>(id);

            _ravenSession.Delete(invoice);

            _ravenSession.SaveChanges();

            return RedirectToAction("Index");
        }

        private string GetNextInvoiceNumber()
        {
            var invoiceNumber = "0001";

            var lastInvoice = _ravenSession.Query<Invoice>().OrderByDescending(i => i.DateCreated).FirstOrDefault();

            if (lastInvoice != null)
            {
                var lastInvoiceNumber = int.Parse(lastInvoice.Number.Replace(string.Concat(WebsiteSettings.InvoiceNumberPrefix, "-"), string.Empty));
                invoiceNumber = (lastInvoiceNumber + 1).ToString(CultureInfo.InvariantCulture);

                for (var x = invoiceNumber.Length; x < 4; x++)
                    invoiceNumber = string.Concat("0", invoiceNumber);
            }

            return string.Format("{0}-{1}", WebsiteSettings.InvoiceNumberPrefix, invoiceNumber);
        }

        private List<SelectListItem> GetClientDropDowns(string clientId = "")
        {
            var clients = _ravenSession.Query<Client>().ToList();
            var clientDropDowns = clients.Select(b => new SelectListItem { Text = b.Name, Value = b.Id.ToString() }).ToList();
            clientDropDowns.Insert(0, new SelectListItem { Text = "Select", Value = string.Empty });

            if (!string.IsNullOrEmpty(clientId) && clientDropDowns.Any(b => b.Value == clientId))
                clientDropDowns.First(b => b.Value == clientId).Selected = true;  

            return clientDropDowns;
        }

        private void AddOrUpdateInvoiceItems(Guid invoiceId, string invoiceItems)
        {
            var counter = 0;

            var invoiceItemsList = string.IsNullOrEmpty(invoiceItems) 
                ? new List<InvoiceItem>() 
                : JsonConvert.DeserializeObject<List<InvoiceItem>>(invoiceItems);

            foreach (var invoiceItem in invoiceItemsList)
            {
                invoiceItem.Order = counter;
                invoiceItem.InvoiceId = invoiceId;

                if (invoiceItem.Id == Guid.Empty)
                {
                    _ravenSession.Store(invoiceItem);
                }
                else
                {
                    var existingItem = _ravenSession.Load<InvoiceItem>(invoiceItem.Id);
                    existingItem.Update(invoiceItem);
                }

                counter++;
            }

            _ravenSession.SaveChanges();

            CleanUpItems(invoiceId, invoiceItemsList);
        }

        private void CleanUpItems(Guid invoiceId, IEnumerable<InvoiceItem> invoiceItemsList)
        {
            var itemIds = invoiceItemsList.Select(ii => ii.Id).ToList();
            var deletedItems = _ravenSession.Query<InvoiceItem>().Where(ii => ii.InvoiceId == invoiceId).ToList();

            foreach (var invoiceItem in deletedItems.Where(invoiceItem => !itemIds.Contains(invoiceItem.Id)))
                _ravenSession.Delete(invoiceItem);

            _ravenSession.SaveChanges();
        }
    }
}