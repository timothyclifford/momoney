﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MoMoney.Models.Domain;
using Raven.Client;

namespace MoMoney.Website.Controllers
{
    public class InvoiceItemsController : ApiController
    {
        private readonly IDocumentSession _ravenSession;

        public InvoiceItemsController(IDocumentSession ravenSession)
        {
            _ravenSession = ravenSession;
        }

        public IEnumerable<InvoiceItem> Get()
        {
            using (_ravenSession)
                return _ravenSession.Query<InvoiceItem>().ToList();
        }

        public InvoiceItem Get(Guid id)
        {
            using (_ravenSession)
                return _ravenSession.Load<InvoiceItem>(id);
        }

        public HttpResponseMessage Post(InvoiceItem viewModel)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, viewModel);

            using (_ravenSession)
            {
                _ravenSession.Store(viewModel);
                _ravenSession.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, viewModel);
            }
        }

        public HttpResponseMessage Put(InvoiceItem viewModel)
        {
            if (!ModelState.IsValid)
                return Request.CreateResponse(HttpStatusCode.BadRequest, viewModel);

            using (_ravenSession)
            {
                var invoiceItem = _ravenSession.Load<InvoiceItem>(viewModel.Id);

                invoiceItem.Update(viewModel);
                _ravenSession.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, invoiceItem);
            }
        }

        public bool Delete(Guid id)
        {
            using (_ravenSession)
            {
                var invoiceItem = _ravenSession.Load<InvoiceItem>(id);

                _ravenSession.Delete(invoiceItem);
                _ravenSession.SaveChanges();

                return true;
            }
        }
    }
}
