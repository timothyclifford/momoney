﻿using System;
using System.Runtime.Serialization;
using MoMoney.Models.Domain.Enums;

namespace MoMoney.Models.Domain
{
    public class InvoicePayment : BaseEntity
    {
        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public InvoicePaymentType Type { get; set; }

        public string Note { get; set; }

        public Guid InvoiceId { get; set; }

        public InvoicePayment()
        {
            Date = DateTime.Now;
        }
    }
}