﻿namespace MoMoney.Models.Domain.Enums
{
    public enum InvoiceStatus
    {
        Draft,
        Sent,
        PartiallyPaid,
        FullyPaid,
        Cancelled,
        Deleted
    }
}
