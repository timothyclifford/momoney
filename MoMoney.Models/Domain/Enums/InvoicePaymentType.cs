﻿namespace MoMoney.Models.Domain.Enums
{
    public enum InvoicePaymentType
    {
        BankTransfer,
        Cheque,
        CreditCard,
        PayPal
    }
}
