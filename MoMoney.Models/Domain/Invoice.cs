﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using MoMoney.Models.Domain.Enums;

namespace MoMoney.Models.Domain
{
    public class Invoice : BaseEntity
    {
        public string Number { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateDue { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public InvoiceStatus Status { get; set; }

        [Required]
        public Guid ClientId { get; set; }

        public Invoice()
        {
            DateCreated = DateTime.Now;
            DateDue = DateTime.Today.AddMonths(1);
            Status = InvoiceStatus.Draft;
        }

        public void Update(Invoice invoice)
        {
            Number = invoice.Number;
            DateDue = invoice.DateDue;
            Title = invoice.Title;
            Description = invoice.Description;
            ClientId = invoice.ClientId;
        }
    }
}