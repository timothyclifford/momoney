﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace MoMoney.Models.Domain
{
    public class InvoiceItem : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Cost { get; set; }

        public int Quantity { get; set; }

        public int Order { get; set; }

        public Guid InvoiceId { get; set; }

        public InvoiceItem()
        {
        }

        public void Update(InvoiceItem invoiceItem)
        {
            Name = invoiceItem.Name;
            Description = invoiceItem.Description;
            Cost = invoiceItem.Cost;
            Quantity = invoiceItem.Quantity;
            Order = invoiceItem.Order;
            InvoiceId = invoiceItem.InvoiceId;
        }
    }
}