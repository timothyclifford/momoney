﻿using System.Runtime.Serialization;

namespace MoMoney.Models.Domain
{
    public class ClientContact
    {
        [DataMember(Name = "First name")]
        public string FirstName { get; set; }

        [DataMember(Name = "Last name")]
        public string LastName { get; set; }

        [DataMember(Name = "Email address")]
        public string Email { get; set; }

        [DataMember(Name = "Phone number")]
        public string Phone { get; set; }

        public ClientContact()
        {
        }
    }
}
