﻿using System.Runtime.Serialization;

namespace MoMoney.Models.Domain
{
    public class ClientAddress
    {
        [DataMember(Name = "Address line one")]
        public string StreetOne { get; set; }

        [DataMember(Name = "Address line two")]
        public string StreetTwo { get; set; }

        [DataMember(Name = "Suburb")]
        public string Suburb { get; set; }

        [DataMember(Name = "State")]
        public string State { get; set; }

        [DataMember(Name = "Country")]
        public string Country { get; set; }

        [DataMember(Name = "Postcode")]
        public string Postcode { get; set; }

        public ClientAddress()
        {
        }
    }
}
