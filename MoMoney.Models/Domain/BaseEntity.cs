﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MoMoney.Models.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }

        protected BaseEntity()
        {
        }
    }
}
