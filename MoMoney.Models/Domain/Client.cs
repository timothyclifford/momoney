﻿namespace MoMoney.Models.Domain
{
    public class Client : BaseEntity
    {
        public string Name { get; set; }

        public string BusinessId { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }

        public ClientAddress Address { get; set; }

        public ClientContact Contact { get; set; }

        public Client()
        {
            Address = new ClientAddress();
            Contact = new ClientContact();
        }

        public void Update(Client client)
        {
            Name = client.Name;
            BusinessId = client.BusinessId;
            Email = client.Email;
            Phone = client.Phone;
            Website = client.Website;
            Address = client.Address;
            Contact = client.Contact;
        }
    }
}