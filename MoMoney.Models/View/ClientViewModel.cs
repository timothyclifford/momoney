﻿using MoMoney.Models.Domain;

namespace MoMoney.Models.View
{
    public class ClientViewModel
    {
        public Client Client { get; set; }

        public ClientViewModel()
        {
            Client = new Client();
        }

        public ClientViewModel(Client client)
        {
            Client = client;
        }
    }
}
