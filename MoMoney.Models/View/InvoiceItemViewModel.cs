﻿using MoMoney.Models.Domain;

namespace MoMoney.Models.View
{
    public class InvoiceItemViewModel
    {
        public InvoiceItem InvoiceItem { get; set; }

        public InvoiceItemViewModel()
        {
            InvoiceItem = new InvoiceItem();
        }
    }
}
