﻿using System.Collections.Generic;
using System.Web.Mvc;
using MoMoney.Models.Domain;
using Newtonsoft.Json;

namespace MoMoney.Models.View
{
    public class InvoiceViewModel
    {
        public Invoice Invoice { get; set; }

        public string InvoiceItems { get; set; }

        public IList<SelectListItem> ClientDropDowns { get; set; }

        public InvoiceViewModel()
        {
            Invoice = new Invoice();
            ClientDropDowns = new List<SelectListItem>();
        }

        public InvoiceViewModel(Invoice invoice) : this()
        {
            Invoice = invoice;
        }

        public InvoiceViewModel(Invoice invoice, IList<InvoiceItem> items) : this()
        {
            Invoice = invoice;
            InvoiceItems = JsonConvert.SerializeObject(items);
        }
    }
}
