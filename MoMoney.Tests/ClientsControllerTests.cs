﻿using System;
using System.Linq;
using MoMoney.Models.Domain;
using MoMoney.Models.View;
using MoMoney.Website.Controllers;
using Raven.Client;
using Raven.Client.Document;
using Xunit;

namespace MoMoney.Tests
{
    public class ClientsControllerTests : IDisposable
    {
        private readonly DocumentStore _documentStore;
        private readonly ClientsController _clientsController;
        private readonly IDocumentSession _documentSession;

        private readonly Guid _clientId = Guid.Parse("73BB1EFE-C655-431A-B53B-3D33F8F8C98D");
        private Client _client;

        public ClientsControllerTests()
        {
            _documentStore = new DocumentStore { ConnectionStringName = "RavenDB" };
            _documentStore.Initialize();
            _documentSession = _documentStore.OpenSession();

            _clientsController = new ClientsController(_documentSession);

            Seed();
        }

        private void Seed()
        {
            // Clear out existing test data

            var clients = _documentSession.Query<Client>().ToList();

            foreach (var c in clients)
                _documentSession.Delete(c);

            _documentSession.SaveChanges();

            // Create new test data

            _client = _documentSession.Load<Client>(_clientId);

            if (_client != null)
                return;

            _client = new Client
            {
                Id = _clientId,
                Name = "Name",
                Email = "client@email.com"
            };

            _documentSession.Store(_client);
            _documentSession.SaveChanges();
        }

        [Fact]
        public void IndexReturnsView()
        {
            var result = _clientsController.Index();

            Assert.NotNull(result);
        }

        [Fact]
        public void GetReturnsView()
        {
            var result = _clientsController.Get(_clientId);

            Assert.NotNull(result);
        }

        [Fact]
        public void CreateGetReturnsView()
        {
            var result = _clientsController.Create();

            Assert.NotNull(result);
        }

        [Fact]
        public void CreatePostReturnsView()
        {
            var newClient = new Client { Name = "New client" };

            var viewModel = new ClientViewModel(newClient);

            var result = _clientsController.Create(viewModel);

            Assert.NotNull(result);

            var created = _documentSession.Load<Client>(newClient.Id);

            Assert.NotNull(created);
        }

        [Fact]
        public void EditGetReturnsView()
        {
            var result = _clientsController.Edit(_clientId);

            Assert.NotNull(result);
        }

        [Fact]
        public void EditPostReturnsView()
        {
            var viewModel = new ClientViewModel(_client);

            var result = _clientsController.Edit(viewModel);

            Assert.NotNull(result);
        }

        [Fact]
        public void EditPostUpdatesClient()
        {
            const string newName = "A different name";

            Assert.NotEqual(newName, _client.Name);

            _client.Name = newName;

            var viewModel = new ClientViewModel(_client);

            var result = _clientsController.Edit(viewModel);

            Assert.NotNull(result);

            var updated = _documentSession.Load<Client>(_clientId);

            Assert.Equal(newName, updated.Name);
        }

        [Fact]
        public void DeleteReturnsView()
        {
            var result = _clientsController.Delete(_clientId);

            Assert.NotNull(result);

            var deleted = _documentSession.Load<Client>(_clientId);

            Assert.Null(deleted);
        }

        public void Dispose()
        {
            _documentSession.Dispose();
            _documentStore.Dispose();
        }
    }
}
