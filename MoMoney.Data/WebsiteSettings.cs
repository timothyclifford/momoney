﻿using System.Web.Configuration;

namespace MoMoney.Framework
{
    public static class WebsiteSettings
    {
        public static string InvoiceNumberPrefix = WebConfigurationManager.AppSettings["InvoiceNumberPrefix"] ?? "MM";
    }
}
